package main

import (
	"context"
	"errors"
	"flag"
	"os"
	"os/signal"
	"syscall"
	"time"

	"codeberg.org/koimoe/sshirc/server"
	"github.com/charmbracelet/log"
	"github.com/charmbracelet/ssh"
	yaml "gopkg.in/yaml.v3"
)

var (
	cfg     *Config
	cfgpath = flag.String("conf", "./config.yaml", "configuration file path")
)

func main() {
	var err error

	flag.Parse()

	cfg, err = loadConfig()
	if err != nil {
		log.Fatal(err)
	}

	l := loadLogger()

	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGTERM, os.Interrupt)
	defer stop()

	srv, err := server.NewServer(&cfg.Server, l)
	if err != nil {
		log.Fatal(err)
	}

	go func() {
		if err := srv.Run(); err != nil && !errors.Is(err, ssh.ErrServerClosed) {
			log.Error(err)
		}
		stop()
	}()
	l.Info("sshirc is running",
		"address", cfg.Server.Listen,
	)

	<-ctx.Done()
	ctx, cancel := context.WithTimeout(context.TODO(), time.Second*10)
	ctx, _ = signal.NotifyContext(context.Background(), syscall.SIGTERM, os.Interrupt)
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		l.Error(err)
	}
	l.Info("sshirc stopped")
}

func loadConfig() (*Config, error) {
	cfg := new(Config)

	f, err := os.Open(*cfgpath)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	if err := yaml.NewDecoder(f).Decode(cfg); err != nil {
		return nil, err
	}
	return cfg, nil
}

func loadLogger() *log.Logger {
	l := log.NewWithOptions(os.Stderr,
		log.Options{ReportTimestamp: true},
	)

	if cfg.Debug {
		l.SetLevel(log.DebugLevel)
		l.SetReportCaller(true)
		l.Debug(cfg)
	}

	return l
}

package main

import (
	srvconf "codeberg.org/koimoe/sshirc/server/config"
)

type Config struct {
	Debug  bool           `yaml:"debug"`
	Server srvconf.Config `yaml:"server"`
}

package auth

import (
	"github.com/charmbracelet/log"
	"github.com/charmbracelet/ssh"
)

func PublickeyAuth() ssh.PublicKeyHandler {
	return func(ctx ssh.Context, key ssh.PublicKey) bool {
		l := log.FromContext(ctx).With(
			"user", ctx.User(),
		)

		ctx.SetValue(log.ContextKey, l)

		l.Debug(key)
		// ssh.KeysEqual(key, authkey)

		return true
	}
}

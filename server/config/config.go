package config

import (
	"context"

	"github.com/charmbracelet/ssh"
	"github.com/charmbracelet/wish"
)

type Config struct {
	Listen  string `yaml:"listen"`
	Hostkey string `yaml:"hostkey"`
}

var ConfigKey = struct{ key string }{"config"}

func Middleware(cfg *Config) wish.Middleware {
	return func(next ssh.Handler) ssh.Handler {
		return func(sess ssh.Session) {
			sess.Context().SetValue(ConfigKey, cfg)
			next(sess)
		}
	}

}

func FromContext(ctx context.Context) *Config {
	raw, _ := ctx.Value(ConfigKey).(*Config)

	return raw
}

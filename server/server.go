package server

import (
	"context"
	"net"

	"codeberg.org/koimoe/sshirc/server/auth"
	srvconf "codeberg.org/koimoe/sshirc/server/config"
	"github.com/charmbracelet/log"
	"github.com/charmbracelet/ssh"
	"github.com/charmbracelet/wish"
	rm "github.com/charmbracelet/wish/recover"
)

type Server struct {
	SSHServer *ssh.Server
	Config    *srvconf.Config
	Logger    *log.Logger
}

func NewServer(config *srvconf.Config, l *log.Logger) (*Server, error) {
	var (
		err error
		srv = new(Server)
	)

	mws := []wish.Middleware{rm.MiddlewareWithLogger(l,
		srvconf.Middleware(config),
	)}

	srv.Config = config
	srv.Logger = l

	srv.SSHServer, err = wish.NewServer(
		wish.WithMiddleware(
			mws...,
		),
		wish.WithPublicKeyAuth(auth.PublickeyAuth()),
		wish.WithAddress(config.Listen),
		wish.WithHostKeyPath(config.Hostkey),
	)

	srv.SSHServer.ConnCallback = srv.connCallback

	if err != nil {
		return nil, err
	}

	return srv, nil
}

func (srv *Server) Run() error {
	return srv.SSHServer.ListenAndServe()
}

func (srv *Server) Shutdown(ctx context.Context) error {
	return srv.SSHServer.Shutdown(ctx)
}

func (srv *Server) connCallback(ctx ssh.Context, conn net.Conn) net.Conn {
	l := srv.Logger.With(
		"addr", conn.RemoteAddr(),
	)
	ctx.SetValue(log.ContextKey, l)

	return conn
}
